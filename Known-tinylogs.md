This is an unofficial list of known tinylogs respecting (mostly) the rfc format:

* ~~gemini://gem.chriswere.uk/nano.gmi ChrisWere~~ (doesn't exist anymore)
* gemini://hexdsl.co.uk/micro.gmi hexdsl
* gemini://adele.pollux.casa/tinylog.gmi Adële
* gemini://szczezuja.space/tinylog.gmi szczezuja
* gemini://frrobert.net/microblog.gmi frrobert
* gemini://rawtext.club/~deerbard/tinylog.gmi Deerbard
* gemini://capsule.sakrajda.eu/tinytinylog.gmi Sakrajda
* gemini://guillaume.pollux.casa/tinylog.gmi Guillaume
* gemini://gmi.bacardi55.io/tinylog.gmi bacardi55
* gemini://anjune.lol/cliplog.gmi monolalia
* gemini://gemini.spelk.online/drek.gmi Drek
* gemini://tobykurien.com/microblog.gmi Toby
* gemini://gmi.antonio.is/nano.gmi Antonio
* gemini://friendo.monster/tiny.gmi Drew
* gemini://caolan.uk/micro.gmi Caolan
* gemini://lyk.so/tiny.gmi Lykso
* gemini://geminids.ga/tinylog.gmi Reaton
* gemini://hedy.tilde.cafe/tinylog.gmi hedy
* gemini://rawtext.club/~samhunter/tinylog.gmi samhunter
* gemini://phreedom.club/~tolstoevsky/tinylog.gmi  tolstoevsky

Tinylogs on the spartan protocol:
* spartan://mozz.us/~mozz/splog.gmi mozz
* spartan://hedy.tilde.cafe:3333/tinylog-mirror.gmi hedy
